let Company = (function func() {
    const tokenValue = 'secret token';
    let passwordValue = Symbol('password')
    
    class Company {
        #users = [];
        #createUserCallBackList = [];
        #updateUserCalLBackList = [];
        #notiftyCallBackList = [];

        constructor(name, maxSize) {
            this.name = name;
            this.maxSize = maxSize;
        }

        static createSuperAdmin(company) {
            if (company.getUsers()[0]?.getToken() === tokenValue) {
                throw new Error('This company has SuperUser!');
            } else {
                let password = prompt('Secret password will be...', '');
                let superUser = new User.prototype.constructor('Super', 'User', true);  
                superUser[passwordValue] = password;
                superUser.company = company;
                company.addSuperUser(superUser)
                company.getCreateUserCallBackList().forEach(cb => cb(superUser))
                return superUser;
            }
        }

        get curSize() {
            return this.#users.length;  
        }

        getUser(index) {
            let user = this.#users.find(item => item.id === index);
            if (!user) {
                throw new Error(`There isnt user with id:${index}`);
            } 
            return user;
        }

        getUsers() {
            return this.#users;
        }

        setUsers(users) {
            this.#users = users;
        }

        addUser(user) {
            this.#users.push(user);
        }

        addSuperUser(superUser) {
            this.#users.unshift(superUser);
        }

        registerUserCreateCallback () {
           Object.values(arguments).forEach(cb => {
               this.#createUserCallBackList.push(cb);
           })
        }
       
        registerUserUpdateCallback() {
            Object.values(arguments).forEach(cb => {
                this.#updateUserCalLBackList.push(cb);
            })
        }

        registerNotifyCallback() {
            Object.values(arguments).forEach(cb => {
                this.#notiftyCallBackList.push(cb);
            })
        }

        getUpdateUserCallBackList() {
            return this.#updateUserCalLBackList;
        }

        getCreateUserCallBackList() {
            return this.#createUserCallBackList;
        }

        getNotifyCallBackList() {
            return this.#notiftyCallBackList;
        }
    }

    let idCounter = 0

    class User {
        #id = this.#generateId();      
        #updateCallBackList = [];
        constructor(name, lastName, isAdmin) {
            this.name = name;
            this.lastName = lastName;
            this.role = null;    
            this.isAdmin = isAdmin;
            if (isAdmin) {
                    this.__token = 'secret token';
                    this.role = 'SUPER USER'
                    this.createUser =function(name, lastName) { 
                        let company = this.company;                           
                        let askPassword = prompt('Show me your password', '');

                        if (this[passwordValue] !== askPassword && this._token !== tokenValue) {
                            throw new Error('You are not authorized!');
                        }

                        if (company.getUsers().length >= company.maxSize) {
                            throw new Error('Sorry, but company doesnt need in new people');
                        }

                        let user = new User.prototype.constructor(name, lastName, false);
                        company.getUpdateUserCallBackList().forEach(cb => user.addUpdateCallBack(cb));
                        user.companyName = company.name;
                        company.addUser(user);
                        company.getNotifyCallBackList().forEach(cb => cb(`${user.name} was added to company`))
                     
                        company.getCreateUserCallBackList().forEach(cb => {cb(user)})

                        if (company.getUsers().length === company.maxSize) {
                            company.getNotifyCallBackList().forEach(cb => cb('There are enough people in company'))
                        }

                        return user;
                    };

                    this.deleteUser = function(id) {
                        let company = this.company;
                        let askPassword = prompt('Show me your password', '');

                        if (askPassword !== this[passwordValue] && this._token !== tokenValue) {
                            throw new Error('You are not authorized!');
                        }

                        let findedUser = company.getUsers().find(u => u.id === id);

                        if (findedUser) {
                            let userIndex = company.getUsers().findIndex(u => u.id === id)
                            if (userIndex === 0) {
                                throw new Error('You cant delete an admin')
                            }
                            let tempArr = [
                                ...company.getUsers().slice(0, userIndex),
                                ...company.getUsers().slice(userIndex + 1)
                            ];
                            company.setUsers(tempArr);
                            company.getUpdateUserCallBackList().forEach(cb => cb(findedUser.id))
                            company.getNotifyCallBackList().forEach(cb => cb(`${findedUser.name} was removed from company`))
                        } else {
                            throw new Error(`There is not user with id:${id}`)
                        }
                    };   
            } 
        }

        #generateId() {
            return idCounter++;
        }

        get id() {
            return this.#id;
        }

        setRole (role) {
            if (typeof role !== 'string') {
                throw new Error('You must write a role!');
            } 
            console.log('SET ROLE')
            this.role = role;
            this.#updateCallBackList.forEach(cb => {cb(this)})
        }

        setName(name) {
            if (!name.length) {
                throw new Error('You should to write a name!')
            }
            this.name = name;
            this.#updateCallBackList.forEach(cb => cb(this))
        }

        setLastName(lastName) {
            if (!lastName.length) {
                throw new Error('You should to write a last name!')
            }
            this.lastName = lastName;
            this.#updateCallBackList.forEach(cb => cb(this))
        }

        addUpdateCallBack(cb) {
            this.#updateCallBackList.push(cb);
        }

        getToken() {
            return this.__token;
        }
    }
    return Company
})();

