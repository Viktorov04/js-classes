let company = new Company('Yojji', 4);

function main() {
    CompanyForm.renderCompany(company)
    company.registerUserCreateCallback(CompanyForm.renderUser);
    company.registerUserUpdateCallback(CompanyForm.updateUser);
    company.registerNotifyCallback(CompanyForm.notify);
    admin = Company.createSuperAdmin(company);
    admin.createUser('Nikita', 'Viktorov');
    admin.createUser('Nikita', 'Viktorov');
    admin.createUser('Nikita', 'Viktorov');
}


