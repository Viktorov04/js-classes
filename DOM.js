    let counter = 1;

    class CompanyForm {
        static renderCompany(company) {
            if (!company) {
                throw new Error('There isnt a company!');
            }
            let companyEl = document.createElement('div');
            companyEl.dataset.companyName = company.name
            companyEl.innerHTML = `
            <nav class="navbar navbar-light bg-light d-flex align-items-start ">
            <div>
              <a class="navbar-brand">${company.name}</a>
            </div>
            <div>
                <p class="navbar-brand max-user">Max-users: ${company.maxSize}</p>
            </div>
            <div>
              <p class="navbar-brand cur-user">Current: ${company.curSize}</p>
          </div>
          </nav>
          <table class="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">LastName</th>
                <th scope="col">Role</th>
                <th scope="col">Id</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody class='users'> 
                <template>
                    <th scope="row" class="row-index">0</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </template>
            </tbody>
          </table>
            `;
            document.querySelector('.container').append(companyEl); 
        }
    
        static renderUser(user) {
            if (user.id === 0) {
                return;
            }
            if (!user) {
                throw new Error('There isnt a user!');
            }
        
            let userEl = document.createElement('tr');
            userEl.dataset.userId = user.id;
            userEl.innerHTML = `
                <th class="row-index" scope="row">${counter++}</th>
                <td>${user.name}</td>
                <td>${user.lastName}</td>
                <td>${user.role || ''}</td>
                <td>${user.id}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" data-user-id = ${user.id}
                    onclick="admin.deleteUser(+this.dataset.userId)">
                        Delete
                    </button>
                </td>
            `;
            document.querySelector('.cur-user').innerText = `Current: ${company.curSize} `
            document.querySelector(`.users`).append(userEl);
        }
    
        static updateUser(userData) {
            if(typeof userData === 'object') {
                document.querySelector(`tr[data-user-id = '${userData.id}']`).innerHTML = `
                <th class="row-index" scope="row">${userData.id}</th>
                <td>${userData.name}</td>
                <td>${userData.lastName}</td>
                <td>${userData.role || ''}</td>
                <td>${userData.id}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" data-user-id = ${user.id}
                    onclick="admin.deleteUser(+this.dataset.userId)">
                        Delete
                    </button>
                 </td>           
                `
                return;
            } 
    
            document.querySelector(`tr[data-user-id = '${userData}']`).remove();
            document.querySelector('.cur-user').innerText = `Current: ${company.curSize} `
            let listOfIndexes = document.querySelectorAll('.row-index');
            for (let i = 0; i < company.curSize - 1; i++) {
                listOfIndexes[i].textContent = i + 1;
            } 
            counter = company.curSize;   
        }
    
        static notify(message) {
            let form = document.querySelector('.toast');
            let text =  document.querySelector('.toast-body');
            text.innerText = message;
            form.style.opacity = 1;
            setTimeout(() => {
                form.style.opacity = 0;
            }, 5000)
        }
    }



  